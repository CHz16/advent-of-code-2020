//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "7", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

var canDirectlyContain: [String: [String]] = [:]
var contents: [String: [(bag: String, n: Int)]] = [:]
input.enumerateLines { line, stop in
    let lineComponents = line.components(separatedBy: " contain ")
    if lineComponents[1] == "no other bag" {
        return
    }

    for containedBagSpec in lineComponents[1].components(separatedBy: ", ") {
        let firstSpaceIndex = containedBagSpec.firstIndex(of: " ")!
        let n = Int(containedBagSpec[containedBagSpec.startIndex..<firstSpaceIndex])!
        let containedBag = String(containedBagSpec[containedBagSpec.index(after: firstSpaceIndex)..<containedBagSpec.endIndex])
        canDirectlyContain[containedBag, default: []].append(lineComponents[0])
        contents[lineComponents[0], default: []].append((bag: containedBag, n: n))
    }
}


var containers = Set<String>()
var underConsideration = canDirectlyContain["shiny gold bag", default: []]
while let bag = underConsideration.popLast() {
    if !containers.contains(bag) {
        containers.insert(bag)
        underConsideration.append(contentsOf: canDirectlyContain[bag, default: []])
    }
}
print(containers.count)


// Part 2

print("--------------")

var memoizedBagContents: [String: Int] = [:]
func bagsContained(in bag: String) -> Int {
    if let n = memoizedBagContents[bag] {
        return n
    }

    let n = contents[bag, default: []].map { $0.n * (1 + bagsContained(in: $0.bag)) }.reduce(0, +)
    memoizedBagContents[bag] = n
    return n
}
print(bagsContained(in: "shiny gold bag"))
