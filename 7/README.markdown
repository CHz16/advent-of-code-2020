Day 7: Handy Haversacks
=======================

https://adventofcode.com/2020/day/7

The first part's pretty easy: just start with the shiny gold bag and add to a list every bag that can directly contain it, then add every bag that can directly contain any of those bags to the list, and so on until you've run out of bags. The second part appeared to me like a classic "you'll be recursing real deep, calculating the same stuff over again, unless you remember the results of subproblems" problem, so I threw some memoization at it, but out of curiosity I removed the memoization afterward and it wasn't actually necessary at all, it never actually gets particularly deep (at least in my data set). For reference, the `return n` line in my function executes 27 times with memoization and 117 without, so very much not a big deal. That wasn't the time sink for me anyway, it took me a bit to get the math right in the recursion.
