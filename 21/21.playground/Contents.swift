//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "21", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

var allAllergens: Set<String> = Set(), allIngredients: Set<String> = Set()
var ingredientCounts: [String: Int] = [:]
var potentialIngredientsForAllergens: [String: Set<String>] = [:]
input.enumerateLines { line, stop in
    let sections = line.components(separatedBy: " (contains ")
    let ingredients = Set(sections[0].components(separatedBy: " "))
    allIngredients = allIngredients.union(ingredients)
    let allergens = Set(sections[1].components(separatedBy: ", "))
    allAllergens = allAllergens.union(allergens)

    for allergen in allergens {
        if let potentialIngredientsForAllergen = potentialIngredientsForAllergens[allergen] {
            potentialIngredientsForAllergens[allergen] = potentialIngredientsForAllergen.intersection(ingredients)
        } else {
            potentialIngredientsForAllergens[allergen] = ingredients
        }
    }

    for ingredient in ingredients {
        ingredientCounts[ingredient, default: 0] += 1
    }
}

let potentiallyAllergenicIngredients: Set<String> = potentialIngredientsForAllergens.values.reduce(Set()) { $0.union($1) }
let unallergenicIngredients = allIngredients.subtracting(potentiallyAllergenicIngredients)
print(unallergenicIngredients.map { ingredientCounts[$0]! }.reduce(0, +))


// Part 2

print("--------------")

var unassignedAllergens = allAllergens
var assignedAllergenForIngredients: [String: String] = [:]
outer: while !unassignedAllergens.isEmpty {
    for allergen in unassignedAllergens {
        if potentialIngredientsForAllergens[allergen]!.count == 1 {
            let ingredient = potentialIngredientsForAllergens[allergen]!.first!
            print(ingredient, "->", allergen)
            assignedAllergenForIngredients[ingredient] = allergen
            unassignedAllergens.remove(allergen)
            potentialIngredientsForAllergens[allergen] = nil
            for (otherIngredient, otherAllergens) in potentialIngredientsForAllergens {
                potentialIngredientsForAllergens[otherIngredient] = otherAllergens.filter { $0 != ingredient }
            }
            continue outer
        }
    }
}

print(assignedAllergenForIngredients.sorted { $0.value < $1.value }.map { $0.key }.joined(separator: ","))
