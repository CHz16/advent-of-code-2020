Day 21: Allergen Assessment
===========================

https://adventofcode.com/2020/day/21

A pretty similar one to day 16; part 2 works in exactly the same way where we have a bunch of things and possibilities that they represent, and we just assign each one that only has one possibility and remove it as a possibility from the others until we've assigned everything.

Part 1 was more interesting. The key observation is that because each allergen is found in exactly one ingredient, then that ingredient must appear in every line flagged as containing that allergen, so the list of candidates is the intersection of every one of those lines. However, the question for me was: does that mean that the list of "ingredients can't possibly contain any of the allergens" is exactly the list of ingredients that never appear as a candidate? Off the top of my head, I wasn't convinced that this was necessarily true

However, I decided to plow ahead as though it was true and see what happened, and it gave the right answer for both the test example and my puzzle input, so that was nice
