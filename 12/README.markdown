Day 12: Rain Risk
=================

https://adventofcode.com/2020/day/12

Just parsing movement instructions and keeping track of some positions, no big deal. The only real "hard" part of this is the rotation instruction in part 2, which you need to make some ugly cases for or just deploy sines and cosines. I got the math wrong on this part several times lmao help how do i trig https://twitter.com/CHz16/status/1337628639156846592

* Part 1: 760th place (9:42)
* Part 2: 734th place (20:45)
