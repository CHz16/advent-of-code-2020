//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "12", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

enum Heading {
    case north, south, east, west

    var x: Int {
        switch self {
        case .north:
            return 0
        case .west:
            return -1
        case .south:
            return 0
        case .east:
            return 1
        }
    }

    var y: Int {
        switch self {
        case .north:
            return 1
        case .west:
            return 0
        case .south:
            return -1
        case .east:
            return 0
        }
    }

    var left: Heading {
        switch self {
        case .north:
            return .west
        case .west:
            return .south
        case .south:
            return .east
        case .east:
            return .north
        }
    }

    var right: Heading {
        switch self {
        case .north:
            return .east
        case .east:
            return .south
        case .south:
            return .west
        case .west:
            return .north
        }
    }
}

var x = 0, y = 0
var heading = Heading.east
input.enumerateLines { line, stop in
    let components = line.components(separatedBy: " ")
    let value = Int(components[1])!
    switch components[0] {
    case "N":
        y += value
    case "S":
        y -= value
    case "E":
        x += value
    case "W":
        x -= value
    case "F":
        x += value * heading.x
        y += value * heading.y
    case "L":
        for _ in 0..<(value/90) {
            heading = heading.left
        }
    case "R":
        for _ in 0..<(value/90) {
            heading = heading.right
        }
    default:
        print(line)
    }
}
print(x, y)
print(abs(x) + abs(y))


// Part 2

print("--------------")

let sines = [0, 1, 0, -1]
let cosines = [1, 0, -1, 0]

x = 0
y = 0
var waypointX = 10, waypointY = 1
input.enumerateLines { line, stop in
    let components = line.components(separatedBy: " ")
    let value = Int(components[1])!
    switch components[0] {
    case "N":
        waypointY += value
    case "S":
        waypointY -= value
    case "E":
        waypointX += value
    case "W":
        waypointX -= value
    case "F":
        x += value * waypointX
        y += value * waypointY
    case "L":
        let quarterTurns = value / 90
        (waypointX, waypointY) = (waypointX * cosines[quarterTurns] - waypointY * sines[quarterTurns], waypointX * sines[quarterTurns] + waypointY * cosines[quarterTurns])
    case "R":
        let quarterTurns = value / 90
        (waypointX, waypointY) = (waypointX * cosines[quarterTurns] + waypointY * sines[quarterTurns], -waypointX * sines[quarterTurns] + waypointY * cosines[quarterTurns])
    default:
        print(line)
    }
}
print(x, y)
print(abs(x) + abs(y))
