Day 19: Monster Messages
========================

https://adventofcode.com/2020/day/19

Ah, a hard one! I completely botched this one from start to finish, I'm not gonna post my times and placements lmao

This one gives you a context-free grammar, which I first wrote a full-on parser for for the first time since college, because it seemed like fun. Only to realize after I was done that the input is a very adversarial grammar that destroyed the parser, oops! After that I wrote something that just generates every single possible valid string, of which there are about 2 million so it doesn't take that long, and got part 1 pretty quick after that

Then part 2 was like "here's recursion, now you die." I did actually give it a go trying to evaluate the recursion, before dying, so instead I did what the puzzle wanted me to do and actually attacked the structure of the grammar. And actually that turns out to make it real easy: part 1 will only accept strings of the form `[42, 42, 31]`, where `42` is a string produced by rule `42` and so on, and part 2 will only accept strings of that form with an arbitrary number of `42`s and `31`s, as long as there's at least one of each and more `42`s than `31`s. So to figure out if we an accept a string, all we have to do is calculate every possible product of those two rules (there happen to be 128 of each, all the same length), decompose the string into substrings of that length, check if they're `42`s or `31`s, and then check if the pattern matches one we accept. Not too hard!

Except it didn't work, and it took me at least an hour bashing my face against my keyboard trying to get it to work. The problem was that I started in a playground when it was a parser, took it to a command-line script to finish it, and then went back to a playground when I started the decomposition approach, but I fixed a bug in the string generation in the command-line file that I never copied back into the playground. So that sucked
