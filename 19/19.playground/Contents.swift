//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "19", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


enum Symbol { case nonterminal(Int), terminal(String) }

let sections = input.components(separatedBy: "\n\n")

var grammar: [Int: [[Symbol]]] = [:]
sections[0].enumerateLines { line, stop in
    let components = line.components(separatedBy: ": ")
    let index = Int(components[0])!
    for rule in components[1].components(separatedBy: " | ") {
        var products: [Symbol] = []
        for token in rule.components(separatedBy: " ") {
            if token.contains("\"") {
                products.append(.terminal(token.filter { $0 != "\"" }))
            } else {
                products.append(.nonterminal(Int(token)!))
            }
        }
        grammar[index, default: []].append(products)
    }
}

var memo: [Int: Set<String>] = [:]
func products(for ruleID: Int) -> Set<String> {
    if let p = memo[ruleID] {
        return p
    }

    var currentProducts: Set<String> = Set()
    for rule in grammar[ruleID]! {
        var ruleProducts: Set = Set([""])
        for symbol in rule {
            if case let .terminal(string) = symbol {
                ruleProducts = Set(ruleProducts.map { string + $0 })
            } else if case let .nonterminal(newRuleID) = symbol {
                ruleProducts = Set(ruleProducts.flatMap { product in products(for: newRuleID).map { product + $0 } })
            }
        }
        currentProducts = currentProducts.union(ruleProducts)
    }

    memo[ruleID] = currentProducts
    return currentProducts
}

_ = products(for: 31)
_ = products(for: 42)
let sliceLength = memo[31]!.first!.count

var decompositions: [[Int]] = []
sections[1].enumerateLines { line, stop in
    if line.count % sliceLength != 0 {
        return
    }

    var decomposition: [Int] = []
    var sliceStart = line.startIndex
    while sliceStart < line.endIndex {
        let sliceEnd = line.index(sliceStart, offsetBy: sliceLength)
        let slice = String(line[sliceStart..<sliceEnd])
        if memo[31]!.contains(slice) {
            decomposition.append(31)
        } else if memo[42]!.contains(slice) {
            decomposition.append(42)
        } else {
            return
        }
        sliceStart = sliceEnd
    }
    decompositions.append(decomposition)
}


// Part 1

print(decompositions.filter { $0 == [42, 42, 31] }.count)


// Part 2

print("--------------")

var valid = 0
for decomposition in decompositions {
    if decomposition[0] != 42 {
        continue
    }

    var i = 1
    while i < decomposition.count, decomposition[i] == 42 {
        i += 1
    }
    if i == decomposition.count {
        continue
    }

    var j = i
    while j < decomposition.count, decomposition[j] == 31 {
        j += 1
    }
    if j != decomposition.count {
        continue
    }

    if i > (j - i) {
        valid += 1
    }
}
print(valid)
