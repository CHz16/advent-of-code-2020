#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "17.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


struct Point: Hashable {
    let x, y, z: Int
}

struct Neighbor {
    let xDelta, yDelta, zDelta: Int
    func apply(to point: Point) -> Point {
        return Point(x: point.x + xDelta, y: point.y + yDelta, z: point.z + zDelta)
    }
}

var neighbors: [Neighbor] = []
for x in -1...1 {
    for y in -1...1 {
        for z in -1...1 {
            if x != 0 || y != 0 || z != 0 {
                neighbors.append(Neighbor(xDelta: x, yDelta: y, zDelta: z))
            }
        }
    }
}


var initialActiveCubes: Set<Point> = Set()
for (y, line) in input.components(separatedBy: "\n").enumerated() {
    for (x, char) in line.enumerated() {
        if char == "#" {
            initialActiveCubes.insert(Point(x: x, y: y, z: 0))
        }
    }
}

var activeCubes = initialActiveCubes
for _ in 0..<6 {
    var scores: [Point: Int] = [:]
    for activeCube in activeCubes {
        for neighbor in neighbors {
            scores[neighbor.apply(to: activeCube), default: 0] += 1
        }
    }

    var nextCubes: Set<Point> = Set()
    for (point, score) in scores {
        let isActive = activeCubes.contains(point)
        if isActive && (score == 2 || score == 3) {
            nextCubes.insert(point)
        } else if !isActive && score == 3 {
            nextCubes.insert(point)
        }
    }
    activeCubes = nextCubes
}
print(activeCubes.count)
