Day 17: Conway Cubes
====================

https://adventofcode.com/2020/day/17

After I saw this was another cellular automaton, I started it directly in a command-line script instead of a playground, but it turns out you only ever execute six steps so maybe I didn't need to!

I really struggled with the problem description of this one, because the algorithm made sense to me but the example that was worked through seemed blatantly incorrect and I couldn't make sense of it. It turns out that that's because blank rows/columns were stripped off for economy of space, and the active cells actually shifted from one time step to the next, so the upper left corner at t=0 was (x: 0, y: 0) and at t=1 was (x: 0, y: 1). Very cool, not even the tiniest bit confusing. I ended up just programming what I understood the algorithm to be and got the correct answer, I only figured out later what was going on with the example.

For this I randomly decided to do something different: instead of storing the state as a 3D array or hash map, I instead just keep a set of all active points. We can then calculate the next step without iterating through coordinates, but instead going through each active point and adding 1 to each of its neighbors in a score matrix, and then we go through every point that has a score and see if it should be active in the next generation based on its score and whether it's inactive or active currently. I'm not sure if this is actually a better approach or not, but it made extending it to four dimensions for part 2 extremely easy! All I had to do was add the extra dimension to the data structures and it just took care of itself

* Part 1: 1644th place (39:25)
* Part 2: 1310th place (41:11)
