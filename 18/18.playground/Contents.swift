//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "18", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

enum Token: Equatable {
    case add, multiply, group, value(Int)

    var precedence: Int {
        switch self {
        case .multiply:
            return 0
        case .add:
            return 10
        case .group, .value:
            return -1
        }
    }

    func apply(_ a: Int, _ b: Int) -> Int {
        switch self {
        case .add:
            return a + b
        case .multiply:
            return a * b
        default:
            return 0
        }
    }
}

var results = 0
input.enumerateLines { line, stop in
    var resultStack = [0]
    var operatorStack = [Token.add]
    for char in line {
        switch char {
        case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9":
            let value = Int(String(char))!
            resultStack[resultStack.count - 1] = operatorStack.popLast()!.apply(resultStack.last!, value)
        case "+":
            operatorStack.append(.add)
        case "*":
            operatorStack.append(.multiply)
        case "(":
            resultStack.append(0)
            operatorStack.append(.add)
        case ")":
            let value = resultStack.popLast()!
            resultStack[resultStack.count - 1] = operatorStack.popLast()!.apply(resultStack.last!, value)
        default:
            continue
        }
    }
    results += resultStack[0]
}
print(results)


// Part 2

print("--------------")

results = 0
input.enumerateLines { line, stop in
    var tokenStack: [Token] = []
    var operatorStack: [Token] = []
    for char in line {
        switch char {
        case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9":
            let value = Int(String(char))!
            tokenStack.append(.value(value))
        case "(":
            operatorStack.append(.group)
        case ")":
            while let op = operatorStack.popLast(), op != .group {
                tokenStack.append(op)
            }
        case "+", "*":
            let op = (char == "+") ? Token.add : Token.multiply
            while !operatorStack.isEmpty && operatorStack.last!.precedence >= op.precedence {
                tokenStack.append(operatorStack.popLast()!)
            }
            operatorStack.append(op)
        default:
            continue
        }
    }
    tokenStack.append(contentsOf: operatorStack.reversed())

    var resultStack: [Int] = []
    for token in tokenStack {
        switch token {
        case let .value(value):
            resultStack.append(value)
        case .add, .multiply:
            let value = resultStack.popLast()!
            resultStack[resultStack.count - 1] = token.apply(resultStack.last!, value)
        default:
            continue
        }
    }
    results += resultStack[0]
}
print(results)
