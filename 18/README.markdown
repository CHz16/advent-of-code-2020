Day 18: Operation Order
=======================

https://adventofcode.com/2020/day/18

Pretty simple infix mathematical expression parsing today. Part 1 I just busted out completely freehand, but since part 2 actually has operator precedence, I had to look up how to implement the shunting-yard algorithm for it. I was surprised to have lost 600 places, I guess everyone else either did it correctly the first time or just leaned on a parser.

* Part 1: 277th place (12:11)
* Part 2: 848th place (34:48)
