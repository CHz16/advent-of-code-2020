Day 9: Encoding Error
=====================

https://adventofcode.com/2020/day/9

Pretty simple problems that consider shifting windows along a stream of data. The first part I didn't bother to do efficiently since the window size is just 5 for the test case and 25 for the actual problem, so we'll just throw `.contains` at an array why not. Part 2 I did bother to spend a little more effort on by using a rolling sum and saving the start and end indices of the window under consideration. The code very much assumes that an answer exists for both parts, don't worry about it

* Part 1: 653rd place (5:47)
* Part 2: 329th place (9:16)
