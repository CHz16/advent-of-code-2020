//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "9", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


let PREAMBLE_LENGTH = 25


// Part 1

let stream = input.components(separatedBy: "\n").compactMap { Int($0) }
var window = Array(stream[0..<PREAMBLE_LENGTH])
var target = -1
for n in stream[PREAMBLE_LENGTH...] {
    var foundSum = false
    for x in window {
        if n != 2 * x && window.contains(n - x) {
            foundSum = true
            break
        }
    }
    if foundSum {
        window.removeFirst()
        window.append(n)
    } else {
        target = n
        print(n)
        break
    }
}


// Part 2

print("--------------")

var start = 0, end = 0, currentSum = stream[0]
while currentSum != target {
    if currentSum < target {
        end += 1
        currentSum += stream[end]
    } else if currentSum > target {
        currentSum -= stream[start]
        start += 1
    }
}
let frame = stream[start...end]
print(frame)
print(frame.min()! + frame.max()!)
