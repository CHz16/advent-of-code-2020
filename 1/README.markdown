Day 1: Report Repair
====================

https://adventofcode.com/2020/day/1

Starting off pretty simply with 2SUM and 3SUM. I did the first part "nicely" with a set to check entries we'd scanned already, which didn't really work for 3SUM so I just had to do a triply-nested loop oops oh well.

The server got hammered during this puzzle and I got 503 errors while trying to submit, so my placement here may not be "accurate." Not that that really matters though, competition will probably be fierce again and I won't ever place again this year

* Part 1: 267th place (7:07)
* Part 2: 918th place (9:25)
