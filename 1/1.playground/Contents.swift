//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "1", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


let target = 2020

// Part 1

var entries = Set<Int>()
input.enumerateLines { line, stop in
    let entry = Int(line)!
    if entries.contains(target - entry) {
        print(entry, "x", target - entry, "=", entry * (target - entry))
    }
    entries.insert(entry)
}


// Part 2

print("--------------")

outer: for a in entries {
    for b in entries {
        if a == b {
            continue
        }
        for c in entries {
            if a == c || b == c {
                continue
            }
            if target == a + b + c {
                print(a, "x", b, "x", c, "=", a * b * c)
                break outer
            }
        }
    }
}
