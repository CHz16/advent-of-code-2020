Day 6: Custom Customs
=====================

https://adventofcode.com/2020/day/6

Very simple one to do with set operations: part 1 is the union of all the lines in a group and part 2 is the intersection. Didn't quite figure out that framing until after I had stopped to think a little about part 2, which cost me a little time. I also started a few minutes late here, not sure I would've placed anyway

* Part 1: 3411th place (9:24)
* Part 2: 2362nd place (14:27)
