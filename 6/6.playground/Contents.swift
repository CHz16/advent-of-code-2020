//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "6", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

var allYesses = 0
for group in input.components(separatedBy: "\n\n") {
    var questions: Set<Character>?
    group.enumerateLines { line, stop in
        if questions == nil {
            questions = Set(line)
        } else {
            questions = questions!.union(line)
        }
    }
    allYesses += questions!.count
}
print(allYesses)


// Part 2

print("--------------")

var sharedYesses = 0
for group in input.components(separatedBy: "\n\n") {
    var sharedQuestions: Set<Character>?
    group.enumerateLines { line, stop in
        if sharedQuestions == nil {
            sharedQuestions = Set<Character>(line)
        } else {
            sharedQuestions = sharedQuestions!.intersection(line)
        }
        if sharedQuestions!.isEmpty {
            stop = true
        }
    }
    sharedYesses += sharedQuestions!.count
}
print(sharedYesses)
