//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "13", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

let lines = input.components(separatedBy: "\n")
let timestamp = Int(lines[0])!
let busIDs = lines[1].components(separatedBy: ",").compactMap { Int($0) }

let nextTimes = busIDs.map { (id: $0, time: ((timestamp - 1) / $0 + 1) * $0) }
let nextBus = nextTimes.min { $0.time < $1.time }!
print(nextBus)
print(nextBus.id * (nextBus.time - timestamp))


// Part 2

print("--------------")

let remainders = lines[1].components(separatedBy: ",").enumerated().filter { $0.1 != "x" }.map { (offset: Int, element: String) -> (modulus: Int, remainder: Int) in
    let modulus = Int(element)!
    return (modulus: modulus, remainder: (modulus - offset % modulus) % modulus)
}
print(remainders)

var modulus = remainders[0].modulus, remainder = remainders[0].remainder
for (nextModulus, nextRemainder) in remainders.dropFirst() {
    let combinedModulus = modulus * nextModulus
    while remainder % nextModulus != nextRemainder {
        remainder = (remainder + modulus) % combinedModulus
    }
    modulus = combinedModulus
}
print(remainder)
