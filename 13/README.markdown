Day 13: Shuttle Search
======================

https://adventofcode.com/2020/day/13

Hey, I placed this year! That's because once again the answer to a puzzle was just to use the Chinese remainder theorem, so I found a solver instead of actually coding it. I did code it later as a bonus, but we hit integer overflow on the puzzle data and I stopped caring so oh well

UPDATE: Afterward I decided to actually implement a CRT solver this time, in the smart way using the Euclidean algorithm, but regrettably the intermediate multiplications overflow 64-bit integers on the puzzle input so RIP. I decided to instead do it with a brute-forcey way that depends on the sum of the moduli, so it runs perfectly fine on the puzzle input, but if there were just say a few six-digit numbers or something it would suck

* Part 1: 176th place (4:53)
* Part 2: 54th place (12:05)
