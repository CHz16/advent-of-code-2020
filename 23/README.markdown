Day 23: Crab Cups
=================

https://adventofcode.com/2020/day/23

Part 1 I was like "I just want to do this entirely in place in one array and spent a bit of time throwing module math everywhere to make a ring buffer-y thing work, and then part was like "actually you need a linked list lmao gotem"

I really didn't feel like programming a linked list class, though, so instead I just used a Dictionary, which is definitely fine don't worry about it

UPDATE: the reason I used a Dictionary because the list has a million nodes, and I didn't feel like allocating all that up front. Dictionary default values let us only create an entry for node *N* once we've changed its pointer to something other than node *N+1*, and I just thought that was cool. But we do end up going through every node repeatedly over the course of part 2, so I made an actual Array version in `23-2.swift` and it's eight seconds faster lmao c'est la vie
