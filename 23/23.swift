#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "23.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

let startingCups = input.compactMap { Int(String($0)) }
func q(_ index: Int) -> Int {
    return index % startingCups.count
}

var cups = startingCups
var start = 0
for _ in 0..<100 {
    let moved1 = cups[q(start + 1)], moved2 = cups[q(start + 2)], moved3 = cups[q(start + 3)]
    var destinationNumber = cups[start] - 1
    if destinationNumber == 0 {
        destinationNumber = cups.count
    }
    for _ in 0..<3 {
        if destinationNumber == moved1 || destinationNumber == moved2 || destinationNumber == moved3 {
            destinationNumber -= 1
            if destinationNumber == 0 {
                destinationNumber = cups.count
            }
        }
    }

    var i = q(start + 1)
    while true {
        let next = cups[q(i + 3)]
        cups[q(i)] = next
        i += 1
        if next == destinationNumber {
            break
        }
    }
    cups[q(i)] = moved1
    cups[q(i + 1)] = moved2
    cups[q(i + 2)] = moved3
    start = q(start + 1)
}

var result: [String] = []
for i in cups.firstIndex(of: 1)!..<cups.count {
    result.append(String(cups[q(i + 1)]))
}
print(result.joined())


// Part 2

print("--------------")

let CUP_COUNT = 1000000

var next: [Int: Int] = [:]
for i in 0..<(startingCups.count - 1) {
    next[startingCups[i]] = startingCups[i + 1]
}
next[startingCups.last!] = startingCups.count + 1
next[CUP_COUNT] = startingCups.first!

start = startingCups.first!
for _ in 0..<10000000 {
    let moved1 = next[start, default: start + 1]
    let moved2 = next[moved1, default: moved1 + 1]
    let moved3 = next[moved2, default: moved2 + 1]
    let nextStart = next[moved3, default: moved3 + 1]
    next[start] = nextStart

    var destination = start - 1
    if destination == 0 {
        destination = CUP_COUNT
    }
    for _ in 0..<3 {
        if destination == moved1 || destination == moved2 || destination == moved3 {
            destination -= 1
            if destination == 0 {
                destination = CUP_COUNT
            }
        }
    }

    let afterDestination = next[destination, default: destination + 1]
    next[destination] = moved1
    next[moved3] = afterDestination
    start = nextStart
}
print(next[1]!, next[next[1]!]!)
print(next[1]! * next[next[1]!]!)
