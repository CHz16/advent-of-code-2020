Day 3: Toboggan Trajectory
==========================

https://adventofcode.com/2020/day/3

Well, three days in and I've already forgotten to do an AoC problem when it dropped, so we're off to a fantastic start. It's okay, we're not going to place at all this year anyway.

The problem is very easily done with some modulo math, and part 2 is just doing it four more times, which is a great excuse to deploy more map/reduce
