//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "3", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


typealias Slope = (x: Int, y: Int)
let slopes = [
    (x: 1, y: 1),
    (x: 3, y: 1),
    (x: 5, y: 1),
    (x: 7, y: 1),
    (x: 1, y: 2)
]

var trees: [[Bool]] = []
input.enumerateLines { line, stop in
    trees.append(line.map { $0 == "#" })
}

func treesHit(atSlope slope: Slope) -> Int {
    var x = 0, y = 0
    var treesHit = 0
    while y < trees.count {
        if trees[y][x] {
            treesHit += 1
        }

        x = (x + slope.x) % trees[0].count
        y += slope.y
    }
    return treesHit
}


// Part 1

print(treesHit(atSlope: slopes[1]))


// Part 2

print("--------------")

print(slopes.map { treesHit(atSlope: $0) }.reduce(1) { $0 * $1 })
