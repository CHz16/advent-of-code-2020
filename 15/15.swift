#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "15.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

let startingNumbers = input.components(separatedBy: CharacterSet(charactersIn: ",\n")).compactMap { Int($0) }
var lastSpoken = startingNumbers.dropLast().enumerated().reduce(into: [:]) { $0[$1.1] = $1.0 + 1 }

var lastNumber = startingNumbers.last!
var turn = lastSpoken.count + 1
while turn < 2020 {
    let nextNumber: Int
    if let numberWasLastSpoken = lastSpoken[lastNumber] {
        nextNumber = turn - numberWasLastSpoken
    } else {
        nextNumber = 0
    }
    lastSpoken[lastNumber] = turn
    lastNumber = nextNumber
    turn += 1
}
print(lastNumber)


// Part 2

print("--------------")

while turn < 30000000 {
    let nextNumber: Int
    if let numberWasLastSpoken = lastSpoken[lastNumber] {
        nextNumber = turn - numberWasLastSpoken
    } else {
        nextNumber = 0
    }
    lastSpoken[lastNumber] = turn
    lastNumber = nextNumber
    turn += 1
}
print(lastNumber)
