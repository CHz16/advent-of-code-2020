Day 15: Rambunctious Recitation
===============================

https://adventofcode.com/2020/day/15

So I guess today's puzzle was a performance trap that I completely avoided??? I moved up 550 places between part 1 and part 2 with literally the exact same code, just with the iteration bound increased.

I guess the trap here is keeping the whole list in memory and seeking backward through it every iteration, instead of just keeping a hash table of the last time you've seen a number, which is what I did directly. Hooray for data structures?

* Part 1: 1039th place (13:52)
* Part 2: 478th place (15:36)
