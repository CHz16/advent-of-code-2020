Day 20: Jurassic Jigsaw
=================================

https://adventofcode.com/2020/day/20

Oh hey, here's an interesting one. I had no idea how I would actually tackle this one, so as an exploration I decided to make a set of every single border and find out how many common ones there were. I found that no border was used by more than two tiles, and that there were exactly the number of shared borders necessary for the grid, meaning that there are absolutely no choices to make at any point; if I find two tiles share a border, then they **must** join along that border.

Then I spent two hours actually trying to get that to work, with absolutely abhorrent orientation logic. Code sucks, don't worry about it

Part 2 is then just pattern recognition in the joined image, which is pretty straightforward, except for the part where orientation matters here too, so this code is also absolutely abhorrent and don't worry about it as well

* Part 1: 2511th place (1:55:19)
* Part 2: 994th place (3:22:23)
