//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "20", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

struct Tile {
    let source: [String]
    let borders: [[Bool]], flippedBorders: [[Bool]]

    init(source: [String]) {
        self.source = source
        borders = [
            source[0].compactMap { $0 == "#" },
            source.map { $0.last! == "#" },
            source.last!.reversed().compactMap { $0 == "#" },
            source.reversed().map { $0.first! == "#" }
        ]
        flippedBorders = [
            borders[0].reversed(),
            borders[3].reversed(),
            borders[2].reversed(),
            borders[1].reversed()
        ]
    }

    func bottomBorder(inOrientation orientation: Orientation) -> [Bool] {
        let borders = !orientation.flipped ? self.borders : flippedBorders
        switch orientation.rotation {
        case .zero:
            return borders[2]
        case .ninety:
            return borders[1]
        case .oneEighty:
            return borders[0]
        case .twoSeventy:
            return borders[3]
        }
    }

    func rightBorder(inOrientation orientation: Orientation) -> [Bool] {
        let borders = !orientation.flipped ? self.borders : flippedBorders
        switch orientation.rotation {
        case .zero:
            return borders[1]
        case .ninety:
            return borders[0]
        case .oneEighty:
            return borders[3]
        case .twoSeventy:
            return borders[2]
        }
    }

    func matchingLeft(border: [Bool]) -> Orientation? {
        if border == borders[3] {
            return Orientation(rotation: .zero, flipped: false)
        } else if border == borders[2] {
            return Orientation(rotation: .ninety, flipped: false)
        } else if border == borders[1] {
            return Orientation(rotation: .oneEighty, flipped: false)
        } else if border == borders[0] {
           return Orientation(rotation: .twoSeventy, flipped: false)
        } else if border == flippedBorders[3] {
            return Orientation(rotation: .zero, flipped: true)
        } else if border == flippedBorders[2] {
            return Orientation(rotation: .ninety, flipped: true)
        } else if border == flippedBorders[1] {
            return Orientation(rotation: .oneEighty, flipped: true)
        } else if border == flippedBorders[0] {
           return Orientation(rotation: .twoSeventy, flipped: true)
        }
        return nil
    }

    func matchingUp(border: [Bool]) -> Orientation? {
        if border == borders[0] {
            return Orientation(rotation: .zero, flipped: false)
        } else if border == borders[3] {
            return Orientation(rotation: .ninety, flipped: false)
        } else if border == borders[2] {
            return Orientation(rotation: .oneEighty, flipped: false)
        } else if border == borders[1] {
           return Orientation(rotation: .twoSeventy, flipped: false)
        } else if border == flippedBorders[0] {
            return Orientation(rotation: .zero, flipped: true)
        } else if border == flippedBorders[3] {
            return Orientation(rotation: .ninety, flipped: true)
        } else if border == flippedBorders[2] {
            return Orientation(rotation: .oneEighty, flipped: true)
        } else if border == flippedBorders[1] {
           return Orientation(rotation: .twoSeventy, flipped: true)
        }
        return nil
    }
}

struct Orientation {
    enum Rotation { case zero, ninety, oneEighty, twoSeventy }
    let rotation: Rotation
    let flipped: Bool
}

var tiles: [Int: Tile] = [:]
var seenBorders: Set<[Bool]> = Set()
var commonBorders: Set<[Bool]> = Set()
for tileData in input.components(separatedBy: "\n\n") {
    if tileData.isEmpty {
        break
    }
    let lines = tileData.components(separatedBy: "\n")
    let id = Int(lines[0].components(separatedBy: " ")[1])!
    let tile = Tile(source: Array(lines.dropFirst()))
    tiles[id] = tile

    let borders = tile.borders + tile.flippedBorders
    for border in borders {
        if seenBorders.contains(border) {
            commonBorders.insert(border)
        }
        seenBorders.insert(border)
    }
}

let gridSize = Int(sqrt(Double(tiles.count)))
var workingSet = Array(tiles.keys)
var grid: [Int: [Int: (id: Int, orientation: Orientation)]] = [:]
for (i, tileID) in workingSet.enumerated() {
    let tile = tiles[tileID]!
    let borderCount = tile.borders.filter { commonBorders.contains($0) }.count
    if borderCount == 2 {
        let orientation: Orientation
        let contains = tile.borders.map { commonBorders.contains($0) }
        if contains[1] && contains[2] {
            orientation = Orientation(rotation: .zero, flipped: false)
        } else if contains[0] && contains[1] {
            orientation = Orientation(rotation: .ninety, flipped: false)
        } else if contains[3] && contains[0] {
            orientation = Orientation(rotation: .oneEighty, flipped: false)
        } else {
            orientation = Orientation(rotation: .twoSeventy, flipped: false)
        }

        print("starting with", tileID)
        grid[0, default: [:]][0] = (id: tileID, orientation: orientation)
        workingSet.remove(at: i)
        break
    }
}

row: for row in 1..<gridSize {
    let (upID, upOrientation) = grid[row - 1]![0]!
    let upBorder: [Bool] = tiles[upID]!.bottomBorder(inOrientation: upOrientation).reversed()
    for (i, tileID) in workingSet.enumerated() {
        if let orientation = tiles[tileID]!.matchingUp(border: upBorder) {
            print(row, 0, tileID)
            grid[row, default: [:]][0] = (id: tileID, orientation: orientation)
            workingSet.remove(at: i)
            continue row
        }
    }
}

for row in 0..<gridSize {
    col: for col in 1..<gridSize {
        let (leftID, leftOrientation) = grid[row]![col - 1]!
        let leftBorder: [Bool] = tiles[leftID]!.rightBorder(inOrientation: leftOrientation).reversed()
        for (i, tileID) in workingSet.enumerated() {
            if let orientation = tiles[tileID]!.matchingLeft(border: leftBorder) {
                print(row, col, tileID)
                grid[row]![col] = (id: tileID, orientation: orientation)
                workingSet.remove(at: i)
                continue col
            }
        }
    }
}

print(grid[0]![0]!.id * grid[0]![gridSize - 1]!.id * grid[gridSize - 1]![0]!.id * grid[gridSize - 1]![gridSize - 1]!.id)


// Part 2

print("--------------")

let borderlessSourceSize = tiles.values.first!.source.count - 2
func convert(x: Int, andY y: Int, inOrientation orientation: Orientation) -> (x: Int, y: Int) {
    if !orientation.flipped {
        switch orientation.rotation {
        case .zero:
            return (x: x, y: y)
        case .ninety:
            return (x: y, y: borderlessSourceSize + 1 - x)
        case .oneEighty:
            return (x: borderlessSourceSize + 1 - x, y: borderlessSourceSize + 1 - y)
        case .twoSeventy:
            return (x: borderlessSourceSize + 1 - y, y: x)
        }
    } else {
        switch orientation.rotation {
        case .zero:
            return (x: borderlessSourceSize + 1 - x, y: y)
        case .ninety:
            return (x: borderlessSourceSize + 1 - y, y: borderlessSourceSize + 1 - x)
        case .oneEighty:
            return (x: x, y: borderlessSourceSize + 1 - y)
        case .twoSeventy:
            return (x: y, y: x)
        }
    }
}

let imageSize = gridSize * borderlessSourceSize
enum CellType { case water, signal, monster }
var image = Array(repeating: Array(repeating: CellType.water, count: imageSize), count: imageSize)
for row in 0..<gridSize {
    for col in 0..<gridSize {
        let (id, orientation) = grid[row]![col]!
        for y in 1...borderlessSourceSize {
            for x in 1...borderlessSourceSize {
                let (convertedX, convertedY) = convert(x: x, andY: y, inOrientation: orientation)
                let slice = tiles[id]!.source[convertedY]
                if slice[slice.index(slice.startIndex, offsetBy: convertedX)] == "#" {
                    image[row * borderlessSourceSize + y - 1][col * borderlessSourceSize + x - 1] = .signal
                }
            }
        }
    }
}

// print(image.map { $0.reduce("") { $0 + ($1 ? "#" : ".") } }.joined(separator: "\n"))


let basePattern = """
                  #
#    ##    ##    ###
 #  #  #  #  #  #
"""

let basePatternLines = basePattern.components(separatedBy: "\n")
let basePatternHeight = basePatternLines.count
let basePatternWidth = basePatternLines.map { $0.count }.max()!

struct Point: Hashable { let x, y: Int }
var patterns: [Set<Point>] = Array(repeating: Set(), count: 8)
for (y, line) in basePatternLines.enumerated() {
    for (x, char) in line.enumerated() {
        if char == "#" {
            patterns[0].insert(Point(x: x, y: y))
            patterns[1].insert(Point(x: basePatternHeight - 1 - y, y: x))
            patterns[2].insert(Point(x: basePatternWidth - 1 - x, y: basePatternHeight - 1 - y))
            patterns[3].insert(Point(x: y, y: basePatternWidth - 1 - x))
            patterns[4].insert(Point(x: basePatternWidth - 1 - x, y: y))
            patterns[5].insert(Point(x: basePatternHeight - 1 - y, y: basePatternWidth - 1 - x))
            patterns[6].insert(Point(x: x, y: basePatternHeight - 1 - y))
            patterns[7].insert(Point(x: y, y: x))
        }
    }
}

for (i, pattern) in patterns.enumerated() {
    let xEnd, yEnd: Int
    if i % 2 == 0 {
        xEnd = imageSize - basePatternWidth + 1
        yEnd = imageSize - basePatternHeight + 1
    } else {
        xEnd = imageSize - basePatternHeight + 1
        yEnd = imageSize - basePatternWidth + 1
    }

    var patternCount = 0
    for y in 0..<yEnd {
        for x in 0..<xEnd {
            var invalid = false
            for cell in pattern {
                if image[cell.y + y][cell.x + x] == .water {
                    invalid = true
                    break
                }
            }
            if !invalid {
                patternCount += 1
                for cell in pattern {
                    image[cell.y + y][cell.x + x] = .monster
                }
            }
        }
    }
    if patternCount != 0 {
        print(patternCount, "found")
        break
    }
}

print(image.map { $0.filter { $0 == .signal }.count }.reduce(0, +))
