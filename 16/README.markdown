Day 16: Ticket Translation
==========================

https://adventofcode.com/2020/day/16

Shoutouts to preprocessing puzzle input in a text editor so I don't have to do any string processing beyond splitting on a character, big fan

Part 1's pretty straightforward. For part 2, we first run through each field and find all the columns it could potentially be. Then we find any fields which can only be one column, record that association, remove that column from consideration from the other unassigned fields, and repeat until we've matched everything.

* Part 1: 387th place (9:57)
* Part 2: 374th place (30:28)
