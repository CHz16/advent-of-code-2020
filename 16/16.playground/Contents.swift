//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "16", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

typealias Ranges = (range1: ClosedRange<Int>, range2: ClosedRange<Int>)

let sections = input.components(separatedBy: "\n\n")
var fields: [String: Ranges] = [:]
sections[0].enumerateLines { line, stop in
    let components = line.components(separatedBy: " ")
    fields[components[0]] = Ranges(range1: Int(components[1])!...Int(components[2])!, range2: Int(components[4])!...Int(components[5])!)
}

var validTickets: [[Int]] = []
var invalidSum = 0
sections[2].enumerateLines { line, stop in
    let numbers = line.components(separatedBy: ",").compactMap { Int($0) }
    var invalid = false
    outer: for number in numbers {
        for (_, (range1: range1, range2: range2)) in fields {
            if range1.contains(number) || range2.contains(number) {
                continue outer
            }
        }
        invalidSum += number
        invalid = true
    }
    if !invalid {
        validTickets.append(numbers)
    }
}
print(invalidSum)


// Part 2

print("--------------")

let myTicket = sections[1].components(separatedBy: ",").compactMap { Int($0) }

var potentialColumns: [String: [Int]] = [:]
for fieldName in fields.keys {
    var columns: [Int] = []
    for column in 0..<fields.count {
        var invalid = false
        for ticket in validTickets {
            if !fields[fieldName]!.range1.contains(ticket[column]) && !fields[fieldName]!.range2.contains(ticket[column]) {
                invalid = true
                break
            }
        }
        if !invalid {
            columns.append(column)
        }
    }
    potentialColumns[fieldName] = columns
}

var associations: [String: Int] = [:]
outer: while !potentialColumns.isEmpty {
    for (fieldName, columns) in potentialColumns {
        if columns.count == 1 {
            associations[fieldName] = columns[0]
            potentialColumns[fieldName] = nil
            for fieldName in potentialColumns.keys {
                potentialColumns[fieldName]!.removeAll(where: { $0 == columns[0] })
            }
            continue outer
        }
    }
}

print(associations)
var product = 1
for (fieldName, column) in associations {
    if fieldName.contains("departure") {
        product *= myTicket[column]
    }
}
print(product)
