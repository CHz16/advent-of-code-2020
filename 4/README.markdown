Day 4: Passport Processing
==========================

https://adventofcode.com/2020/day/4

Much more complicated string parsing & validation problem today. Some of the bits here would have been much faster to code in a non-Swift language due to its string business, and I wasted a bunch of time trying to make regexes work. Luckily it didn't matter anyway because I started late, this time because I was actually doing other things instead.
