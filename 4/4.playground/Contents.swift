//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "4", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

func isLenientPassport(_ rawPassport: String) -> Bool {
    var byr, iyr, eyr, pid, cid: Int?
    var hgt, hcl, ecl: String?

    rawPassport.enumerateLines { line, stop in
        for field in line.components(separatedBy: " ") {
            let fieldComponents = field.components(separatedBy: ":")
            switch fieldComponents[0] {
            case "byr":
                byr = Int(fieldComponents[1]) ?? 0
            case "iyr":
                iyr = Int(fieldComponents[1]) ?? 0
            case "eyr":
                eyr = Int(fieldComponents[1]) ?? 0
            case "hgt":
                hgt = fieldComponents[1]
            case "hcl":
                hcl = fieldComponents[1]
            case "ecl":
                ecl = fieldComponents[1]
            case "pid":
                pid = Int(fieldComponents[1]) ?? 0
            case "cid":
                cid = Int(fieldComponents[1]) ?? 0
            default:
                print("unexpected field", field)
            }
        }
    }

    return (byr != nil && iyr != nil && eyr != nil && hgt != nil && hcl != nil && ecl != nil && pid != nil)
}

print(input.components(separatedBy: "\n\n").map(isLenientPassport).filter { $0 }.count)


// Part 2

print("--------------")

func isStrictPassport(_ rawPassport: String) -> Bool {
    var byr, iyr, eyr, cid: Int?
    var hgt, hcl, ecl, pid: String?

    rawPassport.enumerateLines { line, stop in
        for field in line.components(separatedBy: " ") {
            let fieldComponents = field.components(separatedBy: ":")
            switch fieldComponents[0] {
            case "byr":
                byr = Int(fieldComponents[1])
            case "iyr":
                iyr = Int(fieldComponents[1])
            case "eyr":
                eyr = Int(fieldComponents[1])
            case "hgt":
                hgt = fieldComponents[1]
            case "hcl":
                hcl = fieldComponents[1]
            case "ecl":
                ecl = fieldComponents[1]
            case "pid":
                pid = fieldComponents[1]
            case "cid":
                cid = Int(fieldComponents[1])
            default:
                print("unexpected field", field)
            }
        }
    }

    if byr == nil || iyr == nil || eyr == nil || hgt == nil || hcl == nil || ecl == nil || pid == nil {
        return false
    }

    if byr! < 1920 || byr! > 2002 {
        return false
    }

    if iyr! < 2010 || iyr! > 2020 {
        return false
    }

    if eyr! < 2020 || eyr! > 2030 {
        return false
    }

    let heightRegex = try! NSRegularExpression(pattern: #"^(\d+)(cm|in)$"#, options: [])
    var heightValid = false
    heightRegex.enumerateMatches(in: hgt!, options: [], range: NSRange(hgt!.startIndex..<hgt!.endIndex, in: hgt!)) { result, flags, stop in
        let value = Int(hgt![Range(result!.range(at: 1), in: hgt!)!])!
        let unit = hgt![Range(result!.range(at: 2), in: hgt!)!]
        if unit == "cm" && value >= 150 && value <= 193 {
            heightValid = true
        } else if unit == "in" && value >= 59 && value <= 76 {
            heightValid = true
        }
    }
    if !heightValid {
        return false
    }

    let hairColorRegex = try! NSRegularExpression(pattern: #"^#[0-9a-f]{6}$"#, options: [])
    if hairColorRegex.numberOfMatches(in: hcl!, options: [], range: NSRange(hcl!.startIndex..<hcl!.endIndex, in: hcl!)) == 0 {
        return false
    }

    if ecl! != "amb" && ecl! != "blu" && ecl! != "brn" && ecl! != "gry" && ecl! != "grn" && ecl! != "hzl" && ecl! != "oth" {
        return false
    }

    if pid!.count != 9 && (pid!.filter { "0123456789".contains($0) }).count != 9 {
        return false
    }

    return true
}

print(input.components(separatedBy: "\n\n").map(isStrictPassport).filter { $0 }.count)
