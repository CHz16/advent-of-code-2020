Day 10: Adapter Array
=====================

https://adventofcode.com/2020/day/10

Okay, now THIS one was the memoization one. Though I implemented this one with an incremental loop rather than recursing, no particular reason why.

Part 1 is literally just sorting the list and then calculating the differences between each element. For part 2, the number of ways to use an *N* jolt adapter is (the number of ways to use an *N-1* jolt adapter if one exists) + (the number of ways to use an *N-2* jolt adapter if one exists) + (the number of ways to use an *N-3* jolt adapter if one exists), so you can just start at the bottom with 1 way to use a "0 jolt adapter" and work your way up.

* Part 1: 783rd place (6:36)
* Part 2: 154th place (10:25)
