//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "10", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

var adapters = [0] + input.components(separatedBy: "\n").compactMap { Int($0) }.sorted()
adapters.append(adapters.max()! + 3)

var differences: [Int: Int] = [:]
for i in 0..<(adapters.count - 1) {
    differences[adapters[i + 1] - adapters[i], default: 0] += 1
}
print(differences)
print(differences[1]! * differences[3]!)


// Part 2

print("--------------")

var ways: [Int: Int] = [0: 1]
for i in adapters.dropFirst() {
    var n = 0
    if adapters.contains(i - 3) {
        n += ways[i - 3, default: 0]
    }
    if adapters.contains(i - 2) {
        n += ways[i - 2, default: 0]
    }
    if adapters.contains(i - 1) {
        n += ways[i - 1, default: 0]
    }
    ways[i] = n
}
print(ways[adapters.max()!]!)
