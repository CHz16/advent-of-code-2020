#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "11.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


enum State { case floor, empty, occupied }

var plane: [[State]] = []
input.enumerateLines { line, stop in
    plane.append(line.map {
        if $0 == "." {
            return .floor
        } else if $0 == "L" {
            return .empty
        } else if $0 == "#" {
            return .occupied
        } else {
            print("uh oh", $0)
            return .floor
        }
    })
}


// Part 1

var visibleSeats: [[[(row: Int, col: Int)]]] = []
for row in 0..<plane.count {
    var r: [[(row: Int, col: Int)]] = []
    for col in 0..<plane[0].count {
        let s: [(row: Int, col: Int)] = [(row: row - 1, col: col - 1), (row: row - 1, col: col), (row: row - 1, col: col + 1), (row: row, col: col - 1), (row: row, col: col + 1), (row: row + 1, col: col - 1), (row: row + 1, col: col), (row: row + 1, col: col + 1)].filter { $0.row >= 0 && $0.row < plane.count && $0.col >= 0 && $0.col < plane[0].count }
        r.append(s)
    }
    visibleSeats.append(r)
}

var currentPlane = plane
while true {
    var nextPlane = currentPlane
    for row in 0..<nextPlane.count {
        for col in 0..<nextPlane[0].count {
            let adjacentTypes = visibleSeats[row][col].map { currentPlane[$0.row][$0.col] }
            if currentPlane[row][col] == .empty && (adjacentTypes.filter { $0 == .occupied }.isEmpty) {
                nextPlane[row][col] = .occupied
            } else if currentPlane[row][col] == .occupied && (adjacentTypes.filter { $0 == .occupied }.count >= 4) {
                nextPlane[row][col] = .empty
            }
        }
    }

    if nextPlane == currentPlane {
        break
    }
    currentPlane = nextPlane
}
print(currentPlane.map { $0.map { $0 == .occupied ? 1 : 0 }.reduce(0, +) }.reduce(0, +))


// Part 2

print("--------------")

visibleSeats = []
for row in 0..<plane.count {
    var r: [[(row: Int, col: Int)]] = []
    for col in 0..<plane[0].count {
        var s: [(row: Int, col: Int)] = []
        let deltas = [(row: -1, col: -1), (row: -1, col: 0), (row: -1, col: 1), (row: 0, col: -1), (row: 0, col: 1), (row: 1, col: -1), (row: 1, col: 0), (row: 1, col: 1)]
        for (rowDelta, colDelta) in deltas {
            var i = 1
            while true {
                let currentRow = row + i * rowDelta, currentCol = col + i * colDelta
                if currentRow < 0 || currentRow >= plane.count || currentCol < 0 || currentCol >= plane[0].count {
                    break
                }
                if plane[currentRow][currentCol] != .floor {
                    s.append((row: currentRow, col: currentCol))
                    break
                }
                i += 1
            }
        }
        r.append(s)
    }
    visibleSeats.append(r)
}

currentPlane = plane
while true {
    var nextPlane = currentPlane
    for row in 0..<nextPlane.count {
        for col in 0..<nextPlane[0].count {
            let adjacentTypes = visibleSeats[row][col].map { currentPlane[$0.row][$0.col] }
            if currentPlane[row][col] == .empty && (adjacentTypes.filter { $0 == .occupied }.isEmpty) {
                nextPlane[row][col] = .occupied
            } else if currentPlane[row][col] == .occupied && (adjacentTypes.filter { $0 == .occupied }.count >= 5) {
                nextPlane[row][col] = .empty
            }
        }
    }

    if nextPlane == currentPlane {
        break
    }
    currentPlane = nextPlane
}
print(currentPlane.map { $0.map { $0 == .occupied ? 1 : 0 }.reduce(0, +) }.reduce(0, +))

