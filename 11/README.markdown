Day 11: Seating System
======================

https://adventofcode.com/2020/day/11

Time for a simple cellular automaton, oh boy! This is the first day of the year that I didn't use a playground for, because this loops hundreds of thousands of times and that's real slow in one! Though maybe there's a clever way to decompose the problem to make it much faster. I put absolutely no effort whatsoever in trying to figure that out, brute force away!

* Part 1: 601st place (15:23)
* Part 2: 563rd place (25:14)
