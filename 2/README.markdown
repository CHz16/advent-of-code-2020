Day 2: Password Philosophy
==========================

https://adventofcode.com/2020/day/2

Pretty simple string parsing here. Took me a little bit to remember the reduce-a-string-into-a-dictionary-of-character-counts trick for part 1, and then part 2 I wasted time adding an XOR operator to Bool. Was it worth it? Probably not, but I'm not ever going to place this year, just like last year, so why not

* Part 1: 1078th place (6:08)
* Part 2: 1329th place (11:22)
