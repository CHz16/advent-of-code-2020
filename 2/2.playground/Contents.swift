//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "2", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

var validPasswords = 0
input.enumerateLines { line, stop in
    let components = line.components(separatedBy: " ")
    let min = Int(components[0])!, max = Int(components[1])!
    let char = Character(components[2])
    let characterCounts = components[3].reduce(into: [:]) { counts, char in counts[char, default: 0] += 1 }

    let count = characterCounts[char, default: 0]
    if count >= min && count <= max {
        validPasswords += 1
    }
}
print(validPasswords)


// Part 2

print("--------------")

extension Bool {
    static func ^ (_ left: Bool, _ right: Bool) -> Bool {
        return left != right
    }
}

validPasswords = 0
input.enumerateLines { line, stop in
    let components = line.components(separatedBy: " ")
    let pos1 = Int(components[0])! - 1, pos2 = Int(components[1])! - 1
    let char = Character(components[2])

    let startIndex = components[3].startIndex
    if (components[3][components[3].index(startIndex, offsetBy: pos1)] == char) ^ (components[3][components[3].index(startIndex, offsetBy: pos2)] == char) {
        validPasswords += 1
    }
}
print(validPasswords)
