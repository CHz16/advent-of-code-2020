Day 14: Docking Data
====================

https://adventofcode.com/2020/day/14

Well, it's not Advent of Code until I've written an absolutely disgusting reduce statement that should've been rephrased as a for loop or something. This is all the commentary I'll be providing on this solution at this time.

* Part 1: 913th place (15:35)
* Part 2: 453rd place (26:24)
