//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "14", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


let NUMBER_LENGTH = 36


func decompose(int: Int) -> [Bool] {
    var bits = Array(repeating: false, count: NUMBER_LENGTH)
    var int = int
    for i in (0..<NUMBER_LENGTH).reversed() {
        if int == 0 {
            break
        }
        if int % 2 == 1 {
            bits[i] = true
        }
        int /= 2
    }
    return bits
}


// Part 1

var mask = String(repeating: "X", count: NUMBER_LENGTH)
var memory: [Int: Int] = [:]
input.enumerateLines { line, stop in
    let components = line.components(separatedBy: " ")
    if components[0] == "mask" {
        mask = components[2]
    } else {
        memory[Int(components[1])!] = zip(mask, decompose(int: Int(components[3])!)).reduce(0) { (initialResult: Int, nextPartialResult: (Character, Bool)) -> Int in
            switch nextPartialResult.0 {
            case "0":
                return initialResult * 2
            case "1":
                return initialResult * 2 + 1
            case "X":
                return initialResult * 2 + (nextPartialResult.1 ? 1 : 0)
            default:
                print("uh oh", mask)
                return initialResult * 2
            }
        }
    }
}
print(memory.reduce(0) { $0 + $1.value} )


// Part 2

print("--------------")

mask = String(repeating: "X", count: NUMBER_LENGTH)
memory = [:]
input.enumerateLines { line, stop in
    let components = line.components(separatedBy: " ")
    if components[0] == "mask" {
        mask = components[2]
    } else {
        let addresses: [Int] = zip(mask, decompose(int: Int(components[1])!)).reduce([0]) { initialResult, nextPartialResult in
            switch nextPartialResult.0 {
            case "0":
                return initialResult.map { $0 * 2 + (nextPartialResult.1 ? 1 : 0) }
            case "1":
                return initialResult.map { $0 * 2 + 1 }
            case "X":
                return initialResult.map { $0 * 2 } + initialResult.map { $0 * 2 + 1 }
            default:
                print("uh oh", mask)
                return initialResult.map { $0 * 2 }
            }
        }
        for address in addresses {
            memory[address] = Int(components[3])!
        }
    }
}
print(memory.reduce(0) { $0 + $1.value} )
