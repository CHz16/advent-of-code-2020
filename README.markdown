Advent of Code 2020
===================

These are my solutions in Swift to [Advent of Code 2020](https://adventofcode.com/2020), a kind of fun series of 25 programming puzzles that ran in December 2020. I figured I'd throw these up on the web somewhere, because why not. If I was competing for a leaderboard spot that day (that is, I started coding at 9 PM Pacific), then the readme file for that day will give my time and place.

While the challenges had no time limit, there was a leaderboard that showed (essentially) the fastest 100 solvers of each day's problem, so naturally I tried to solve them as quickly as possible. So, most of the solutions here are simply what left my fingers the fastest. That's speedcoding for you.

Every user had their puzzle inputs randomly selected from a pool of possibilities, so the inputs you see in these files may not be the same ones you'd get were you to sign up. I also tend to preprocess them in a text editor to make them way easier to parse.

* Best placement for any part (tie): day 13, part 2 and day 25, part 2 (54th place)
* Best placement for a part that I actually wrote code for: day 24, part 2 (58th place)
* Days with later optimizations or bugfixes: 13, 23

*See also: [2015](https://bitbucket.org/CHz16/advent-of-code-2015), [2016](https://bitbucket.org/CHz16/advent-of-code-2016), [2017](https://bitbucket.org/CHz16/advent-of-code-2017), [2018](https://bitbucket.org/CHz16/advent-of-code-2018), [2019](https://bitbucket.org/CHz16/advent-of-code-2019), [2021](https://bitbucket.org/CHz16/advent-of-code-2021), [2022](https://bitbucket.org/CHz16/advent-of-code-2022)*
