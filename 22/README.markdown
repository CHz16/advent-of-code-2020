Day 22: Crab Combat
===================

https://adventofcode.com/2020/day/22

Absolutely no fancy tricks or algorithms here, besides arguably a gross reduce to calculate the score after a round. I'm sure there's a nice way to optimize this but I sure as hell didn't do that

* Part 1: 442nd place (6:49)
* Part 2: 255th place (26:29)
