#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "22.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

let sections = input.components(separatedBy: "\n\n")
var startingDecks = sections.map { $0.components(separatedBy: "\n").dropFirst().compactMap { Int($0) } }

var decks = startingDecks
while !decks[0].isEmpty && !decks[1].isEmpty {
    let card1 = decks[0].removeFirst(), card2 = decks[1].removeFirst()
    if card1 > card2 {
        decks[0].append(card1)
        decks[0].append(card2)
    } else {
        decks[1].append(card2)
        decks[1].append(card1)
    }
}

if decks[1].isEmpty {
    print("player 1 wins")
    print(zip(decks[0], (1...decks[0].count).reversed()).reduce(0) { $0 + $1.0 * $1.1 })
} else {
    print("player 2 wins")
    print(zip(decks[1], (1...decks[1].count).reversed()).reduce(0) { $0 + $1.0 * $1.1 })
}


// Part 2

print("--------------")

//var seenPositions: Set<[[Int]]> = Set()
func recursiveCombat(_ deck1: [Int], _ deck2: [Int]) -> (player1Wins: Bool, deck1: [Int], deck2: [Int]) {
    var deck1 = deck1, deck2 = deck2
    var seenPositions: Set<[[Int]]> = Set()

    while true {
        if deck1.isEmpty || deck2.isEmpty {
            return (player1Wins: deck2.isEmpty, deck1: deck1, deck2: deck2)
        }

        let position = [deck1, deck2]
        if seenPositions.contains(position) {
            return (player1Wins: true, deck1: deck1, deck2: deck2)
        }
        seenPositions.insert(position)

        let card1 = deck1.removeFirst(), card2 = deck2.removeFirst()
        if card1 > deck1.count || card2 > deck2.count {
            if card1 > card2 {
                deck1.append(card1)
                deck1.append(card2)
            } else {
                deck2.append(card2)
                deck2.append(card1)
            }
        } else {
            let innerDeck1 = Array(deck1[0..<card1]), innerDeck2 = Array(deck2[0..<card2])
            if recursiveCombat(innerDeck1, innerDeck2).player1Wins {
                deck1.append(card1)
                deck1.append(card2)
            } else {
                deck2.append(card2)
                deck2.append(card1)
            }
        }
    }
}

let (player1Wins, recursiveDeck1, recursiveDeck2) = recursiveCombat(startingDecks[0], startingDecks[1])
if player1Wins {
    print("player 1 wins")
    print(zip(recursiveDeck1, (1...recursiveDeck1.count).reversed()).reduce(0) { $0 + $1.0 * $1.1 })
} else {
    print("player 2 wins")
    print(zip(recursiveDeck2, (1...recursiveDeck2.count).reversed()).reduce(0) { $0 + $1.0 * $1.1 })
}
