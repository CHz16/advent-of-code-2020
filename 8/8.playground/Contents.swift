//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "8", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


struct VM {
    enum Instruction { case acc(Int), jmp(Int), nop(Int) }
    enum TerminationReason { case infiniteLoop, pcOutOfBounds(Int) }

    var instructions: [Instruction], executedLine: [Bool]
    var pc = 0
    var acc = 0

    init(program: String) {
        var instructions: [Instruction] = []
        program.enumerateLines { line, stop in
            let components = line.components(separatedBy: " ")
            if components[0] == "acc" {
                instructions.append(.acc(Int(components[1])!))
            } else if components[0] == "jmp" {
                instructions.append(.jmp(Int(components[1])!))
            } else if components[0] == "nop" {
                instructions.append(.nop(Int(components[1])!))
            } else {
                print("unknown line", line)
            }
        }
        self.instructions = instructions
        executedLine = Array(repeating: false, count: instructions.count)
    }

    mutating func execute() -> TerminationReason {
        while pc >= 0 && pc < instructions.count {
            if executedLine[pc] {
                return .infiniteLoop
            }

            executedLine[pc] = true
            switch instructions[pc] {
            case let .acc(value):
                acc += value
            case let .jmp(value):
                pc += value - 1
            case .nop:
                break // pass
            }
            pc += 1
        }

        return .pcOutOfBounds(pc)
    }
}


// Part 1

var vm = VM(program: input)
print(vm.execute())
print(vm.acc)


// Part 2

print("--------------")

let initialVM = VM(program: input)
for i in 0..<initialVM.instructions.count {
    if case .acc(_) = initialVM.instructions[i] {
        continue
    }
    var mutatedVM = initialVM
    if case let .jmp(value) = mutatedVM.instructions[i] {
        mutatedVM.instructions[i] = .nop(value)
    } else if case let .nop(value) = mutatedVM.instructions[i] {
        mutatedVM.instructions[i] = .jmp(value)
    }

    if case let .pcOutOfBounds(pc) = mutatedVM.execute(), pc == mutatedVM.instructions.count {
        print("mutated instruction", i)
        print(mutatedVM.acc)
        break
    }
}
