Day 8: Handheld Halting
=======================

https://adventofcode.com/2020/day/8

Here we go, it's Obligatory VM Time once again here at the Advent of Code programming challenge. Very simple one here with only three instructions, nothing we haven't done in past years.

* Part 1: 5174th place (19:32)
* Part 2: 2707th place (27:17)
