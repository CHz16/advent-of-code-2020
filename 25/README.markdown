Day 25: Combo Breaker
=====================

https://adventofcode.com/2020/day/25

waow placed for the second day in a row, where was this at any other point in AoC this year

Anyway as is tradition, part 1 was a pretty easy finale, and then part 2 was just pushing a button after having solved every other puzzle so it doesn't really count. This one is just modular exponentiation, which in fact was the 2015 finale puzzle as well. I made no attempt at optimizing it with exponentiation by squaring or anything, just threw the CPU at it until it finished

* Part 1: 66th place (6:26)
* Part 2: 54th place (6:30)
