#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "25.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

let keys = input.components(separatedBy: "\n").compactMap { Int($0) }
var loopSizes: [Int] = []
for key in keys {
    var subject = 1
    var loop = 0
    while subject != key {
        subject = (subject * 7) % 20201227
        loop += 1
    }
    loopSizes.append(loop)
}
print(loopSizes)

var subject = 1
for _ in 0..<loopSizes[1] {
    subject = (subject * keys[0]) % 20201227
}
print(subject)
