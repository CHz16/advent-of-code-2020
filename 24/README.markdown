Day 24: Lobby Layout
====================

https://adventofcode.com/2020/day/24

Wowee, I placed with a solution I actually wrote code for for the first time in two years. The secret was knowing to immediately go here to look up how to represent hexagonal coordinates: https://www.redblobgames.com/grids/hexagons/#coordinates

For part 2, instead of worrying how to store hexagonal grid data, I did the exact same thing from day 17 where I just stored a set of the active cells and worked with that. It ran pretty badly in a playground, but not badly enough that I had time to copy the code into a command-line script before it finished

* Part 1: 89th place (6:52)
* Part 2: 58th place (13:24)
