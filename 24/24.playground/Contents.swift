//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "24", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

struct Point: Hashable { let q, r: Int }
var black: Set<Point> = Set()
input.enumerateLines { line, stop in
    let steps = line.components(separatedBy: " ")
    var q = 0, r = 0
    for step in steps {
        if step == "e" {
            q += 1
        } else if step == "se" {
            r += 1
        } else if step == "sw" {
            q -= 1
            r += 1
        } else if step == "w" {
            q -= 1
        } else if step == "nw" {
            r -= 1
        } else if step == "ne" {
            q += 1
            r -= 1
        }
    }

    let point = Point(q: q, r: r)
    if black.contains(point) {
        black.remove(point)
    } else {
        black.insert(point)
    }
}
print(black.count)


// Part 2

print("--------------")

var currentBlack = black
for _ in 0..<100 {
    var counts: [Point: Int] = [:]
    for point in currentBlack {
        counts[Point(q: point.q + 1, r: point.r), default: 0] += 1
        counts[Point(q: point.q, r: point.r + 1), default: 0] += 1
        counts[Point(q: point.q - 1, r: point.r + 1), default: 0] += 1
        counts[Point(q: point.q - 1, r: point.r), default: 0] += 1
        counts[Point(q: point.q, r: point.r - 1), default: 0] += 1
        counts[Point(q: point.q + 1, r: point.r - 1), default: 0] += 1
    }

    var nextBlack: Set<Point> = Set()
    for (point, count) in counts {
        if currentBlack.contains(point) {
            if count == 1 || count == 2 {
                nextBlack.insert(point)
            }
        } else {
            if count == 2 {
                nextBlack.insert(point)
            }
        }
    }
    currentBlack = nextBlack
}
print(currentBlack.count)
