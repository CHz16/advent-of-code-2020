//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "5", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

func binaryDecode(spec: String, withLowChar low: Character) -> Int {
    var min = 0
    var step = Int(pow(2.0, Double(spec.count - 1)))
    for char in spec {
        if char != low {
            min += step
        }
        step /= 2
    }
    return min
}

func seatInfo(forBoardingPass boardingPass: String) -> (row: Int, col: Int, id: Int) {
    let rowStart = boardingPass.startIndex, rowEnd = boardingPass.index(rowStart, offsetBy: 7)
    let colStart = rowEnd, colEnd = boardingPass.endIndex
    let row = binaryDecode(spec: String(boardingPass[rowStart..<rowEnd]), withLowChar: "F")
    let col = binaryDecode(spec: String(boardingPass[colStart..<colEnd]), withLowChar: "L")
    return (row: row, col: col, id: row * 8 + col)
}


var seatIDs = Set<Int>()
var maxID = -1
input.enumerateLines { line, stop in
    let info = seatInfo(forBoardingPass: line)
    seatIDs.insert(info.id)
    if info.id > maxID {
        print(line, info)
        maxID = info.id
    }
}


// Part 2

print("--------------")

for id in seatIDs {
    if !seatIDs.contains(id + 1) && seatIDs.contains(id + 2) {
        print(id + 1)
        break
    }
}
