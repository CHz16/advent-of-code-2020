Day 5: Binary Boarding
======================

https://adventofcode.com/2020/day/5

Binary search, except the binary search has already been done so all you have to do is calculate the result from the movements. Turns out you don't even need to track both the min and the max for this; since they'll both converge to the same number at the end, you can just track one. Had some math wrong so it took me longer than it should've to get this part right.

For part 2, I misunderstood what it was asking and found the first occupied seat with occupied IDs above and below it, instead of the one unoccupied seat with occupied IDs above and below it. Whoops! Still made up 500 places anyway though

* Part 1: 2702nd place (16:00)
* Part 2: 2244th place (20:19)
